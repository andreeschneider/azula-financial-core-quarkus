package com.azula.api.v1.controller;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.azula.api.v1.dto.AccountCreateDto;

@Path(RestPath.BASE_PATH + "/accounts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountController {

    @POST
    public Response create(@Valid AccountCreateDto accountCreateDto) {
        return Response.ok().build();
    }
}
