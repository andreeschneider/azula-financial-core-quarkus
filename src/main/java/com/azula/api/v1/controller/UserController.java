package com.azula.api.v1.controller;

import com.azula.api.v1.dto.UserCreateDto;
import com.azula.api.v1.dto.UserLoginRequestDto;
import com.azula.api.v1.dto.UserLoginRequestResponseDto;
import com.azula.api.v1.dto.UserResponseDto;
import com.azula.api.v1.mapper.UserMapper;
import com.azula.authentication.PBKDF2Encoder;
import com.azula.exception.model.BadCredentialsException;
import com.azula.exception.user.UserNotFoundException;
import com.azula.service.UserService;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path(RestPath.BASE_PATH + "/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserController {

    @Inject
    UserService userService;

    @PermitAll
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(UserCreateDto userCreateDto) {
        return Response.ok(UserMapper.toResponseDto(
                userService.create(userCreateDto)))
                .build();
    }

    @PermitAll
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(UserLoginRequestDto userLoginDto) {

        try {
            UserLoginRequestResponseDto userLoginRequestResponseDto = userService.validateUserCredentials(userLoginDto);

            return Response.ok(userLoginRequestResponseDto).build();
        } catch (UserNotFoundException | BadCredentialsException e) {
            Response.status(Status.UNAUTHORIZED).build();
        }

        return Response.serverError().build();
    }
}
