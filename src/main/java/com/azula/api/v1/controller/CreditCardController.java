package com.azula.api.v1.controller;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.azula.api.v1.dto.CreditCardCreateDto;
import com.azula.api.v1.mapper.CreditCardMapper;
import com.azula.exception.model.BaseException;
import com.azula.service.CreditCardService;

@Path(RestPath.BASE_PATH + "/credit-card")
public class CreditCardController {

    @Inject
    private CreditCardService creditCardService;

    @RolesAllowed("USER")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(CreditCardCreateDto creditCardCreateDto) throws BaseException {
        return Response.ok(CreditCardMapper.toResponseDto(
                creditCardService.create(creditCardCreateDto)))
                .build();
    }
}
