package com.azula.api.v1.controller;

public final class RestPath {

    public static final String BASE_PATH = "/azula-financial/api/v1";

    private RestPath() {
    }
}

