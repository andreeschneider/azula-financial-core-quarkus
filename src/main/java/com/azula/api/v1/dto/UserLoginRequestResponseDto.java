package com.azula.api.v1.dto;

public class UserLoginRequestResponseDto {

    private String token;

    public String getToken() {
        return token;
    }

    public UserLoginRequestResponseDto setToken(String token) {
        this.token = token;
        return this;
    }
}
