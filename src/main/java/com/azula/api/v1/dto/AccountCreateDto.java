package com.azula.api.v1.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.azula.entity.AccountType;

public class AccountCreateDto {

    @NotBlank(message = "${account.create.name.notBlank")
    private String name;

    @NotNull(message = "${account.create.accountType.notNull")
    private AccountType accountType;

    public String getName() {
        return name;
    }

    public AccountCreateDto setName(String name) {
        this.name = name;
        return this;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public AccountCreateDto setAccountType(AccountType accountType) {
        this.accountType = accountType;
        return this;
    }
}
