package com.azula.api.v1.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CreditCardCreateDto {

    @NotBlank(message = "${creditCard.create.userId.notBlank")
    private String userIdentification;

    @NotBlank(message = "${creditCard.create.bankName.notBlank")
    private String bankName;

    @NotBlank(message = "${creditCard.create.invoiceDueDate.notBlank")
    private LocalDate invoiceDueDate;

    @NotNull(message = "${creditCard.create.invoiceDueDate.notBlank")
    private Long invoiceCutDays;

    public String getUserIdentification() {
        return userIdentification;
    }

    public CreditCardCreateDto setUserIdentification(String userIdentification) {
        this.userIdentification = userIdentification;
        return this;
    }

    public String getBankName() {
        return bankName;
    }

    public CreditCardCreateDto setBankName(String bankName) {
        this.bankName = bankName;
        return this;
    }

    public LocalDate getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public CreditCardCreateDto setInvoiceDueDate(LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
        return this;
    }

    public Long getInvoiceCutDays() {
        return invoiceCutDays;
    }

    public CreditCardCreateDto setInvoiceCutDays(Long invoiceCutDays) {
        this.invoiceCutDays = invoiceCutDays;
        return this;
    }
}
