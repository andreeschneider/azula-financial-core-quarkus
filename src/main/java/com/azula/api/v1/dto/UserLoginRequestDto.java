package com.azula.api.v1.dto;

public class UserLoginRequestDto {

    private String userEmail;

    private String userPassword;

    public String getUserEmail() {
        return userEmail;
    }

    public UserLoginRequestDto setUserEmail(String userEmail) {
        this.userEmail = userEmail;
        return this;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public UserLoginRequestDto setUserPassword(String userPassword) {
        this.userPassword = userPassword;
        return this;
    }
}
