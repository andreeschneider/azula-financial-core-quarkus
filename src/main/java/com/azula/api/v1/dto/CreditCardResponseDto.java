package com.azula.api.v1.dto;

import java.time.LocalDate;

public class CreditCardResponseDto {

    private String id;

    private String bankName;

    private LocalDate invoiceDueDate;

    private Long invoiceCutDays;

    private String userIdentification;

    public String getId() {
        return id;
    }

    public CreditCardResponseDto setId(String id) {
        this.id = id;
        return this;
    }

    public String getBankName() {
        return bankName;
    }

    public CreditCardResponseDto setBankName(String bankName) {
        this.bankName = bankName;
        return this;
    }

    public LocalDate getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public CreditCardResponseDto setInvoiceDueDate(LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
        return this;
    }

    public Long getInvoiceCutDays() {
        return invoiceCutDays;
    }

    public CreditCardResponseDto setInvoiceCutDays(Long invoiceCutDays) {
        this.invoiceCutDays = invoiceCutDays;
        return this;
    }

    public String getUserIdentification() {
        return userIdentification;
    }

    public CreditCardResponseDto setUserIdentification(String userIdentification) {
        this.userIdentification = userIdentification;
        return this;
    }
}
