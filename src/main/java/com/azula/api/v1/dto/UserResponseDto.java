package com.azula.api.v1.dto;

public class UserResponseDto {

    private String id;

    private String email;

    private String name;

    public String getId() {
        return id;
    }

    public UserResponseDto setId(String id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserResponseDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserResponseDto setName(String name) {
        this.name = name;
        return this;
    }
}
