package com.azula.api.v1.dto;

import java.time.LocalDate;

public class CreditCardOrderCreateDto {

    private String creditCardId;

    private LocalDate date;

    private String title;

    private Double amount;

    private String responsibleUserId;

    public String getCreditCardId() {
        return creditCardId;
    }

    public CreditCardOrderCreateDto setCreditCardId(String creditCardId) {
        this.creditCardId = creditCardId;
        return this;
    }

    public LocalDate getDate() {
        return date;
    }

    public CreditCardOrderCreateDto setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public CreditCardOrderCreateDto setTitle(String title) {
        this.title = title;
        return this;
    }

    public Double getAmount() {
        return amount;
    }

    public CreditCardOrderCreateDto setAmount(Double amount) {
        this.amount = amount;
        return this;
    }

    public String getResponsibleUserId() {
        return responsibleUserId;
    }

    public CreditCardOrderCreateDto setResponsibleUserId(String responsibleUserId) {
        this.responsibleUserId = responsibleUserId;
        return this;
    }
}
