package com.azula.api.v1.dto;

import javax.validation.constraints.NotBlank;

public class UserCreateDto {

    @NotBlank(message = "${account.create.name.notBlank")
    private String name;

    @NotBlank(message = "${account.create.name.notBlank")
    private String email;

    @NotBlank(message = "${account.create.name.notBlank")
    private String password;

    public String getName() {
        return name;
    }

    public UserCreateDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserCreateDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserCreateDto setPassword(String password) {
        this.password = password;
        return this;
    }
}
