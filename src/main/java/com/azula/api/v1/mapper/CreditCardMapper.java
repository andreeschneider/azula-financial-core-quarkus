package com.azula.api.v1.mapper;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;

import com.azula.api.v1.dto.CreditCardCreateDto;
import com.azula.api.v1.dto.CreditCardResponseDto;
import com.azula.entity.User;
import com.azula.entity.billing.CreditCard;
import com.azula.helper.InvoiceHelper;

public class CreditCardMapper {

    private static final ModelMapper modelMapper = new ModelMapper();

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    static {
        modelMapper.addConverter(new AbstractConverter<LocalDate, String>() {
            @Override
            protected String convert(LocalDate source) {
                DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);
                return source.format(format);
            }
        });
    }

    public static CreditCard toEntity(CreditCardCreateDto creditCardCreateDto, User user) {
        return modelMapper.map(creditCardCreateDto, CreditCard.class)
                .setUser(user);
    }

    public static CreditCardResponseDto toResponseDto(CreditCard creditCard) {
        return modelMapper.map(creditCard, CreditCardResponseDto.class)
                .setUserIdentification(creditCard.getUser().getId());
    }
}
