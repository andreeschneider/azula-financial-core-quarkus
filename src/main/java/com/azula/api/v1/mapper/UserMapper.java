package com.azula.api.v1.mapper;

import com.azula.api.v1.dto.UserCreateDto;
import com.azula.api.v1.dto.UserResponseDto;
import com.azula.entity.User;
import org.modelmapper.ModelMapper;

public class UserMapper {

    private static final ModelMapper modelMapper = new ModelMapper();

    public static User toEntity(UserCreateDto userCreateDto){
        return modelMapper.map(userCreateDto, User.class);
    }

    public static UserResponseDto toResponseDto(User user){
        return  modelMapper.map(user, UserResponseDto.class);
    }
}
