package com.azula.api.v1.mapper;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;

import com.azula.api.v1.dto.AccountCreateDto;
import com.azula.entity.Account;

public class AccountMapper {

    private static final ModelMapper modelMapper = new ModelMapper();

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    static {
        modelMapper.addConverter(new AbstractConverter<LocalDate, String>() {
            @Override
            protected String convert(LocalDate source) {
                DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);
                return source.format(format);
            }
        });
    }

    public static Account toEntity(AccountCreateDto accountCreateDto){
        return null;
    }

}
