package com.azula.service;

import java.time.LocalDate;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.azula.entity.billing.Invoice;

@ApplicationScoped
public class InvoiceService {

    private List<Invoice> createInvoices(LocalDate dueDate, Integer cutDays) {
        return null;
    }

    private Invoice build(Long month, LocalDate dueDate) {
        LocalDate startingAt = dueDate
                .minusMonths(month)
                .plusDays(1);

        return new Invoice()
                .setPeriodStart(startingAt)
                .setPeriodEnd(dueDate.plusMonths(1));
    }
}
