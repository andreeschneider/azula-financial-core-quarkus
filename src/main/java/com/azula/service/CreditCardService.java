package com.azula.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import com.azula.api.v1.dto.CreditCardCreateDto;
import com.azula.api.v1.mapper.CreditCardMapper;
import com.azula.entity.User;
import com.azula.entity.billing.CreditCard;
import com.azula.exception.model.BaseException;
import com.azula.exception.user.UserNotFoundException;
import com.azula.repository.CreditCardRepository;

@ApplicationScoped
public class CreditCardService {

    @Inject
    private CreditCardRepository creditCardRepository;

    @Inject
    private UserService userService;

    @Transactional(TxType.REQUIRED)
    public CreditCard create(CreditCardCreateDto creditCardCreateDto) throws BaseException {
        CreditCard creditCard = CreditCardMapper.toEntity(creditCardCreateDto, associateUser(creditCardCreateDto.getUserIdentification()));

        creditCardRepository.persist(creditCard);

        return creditCard;
    }

    private User associateUser(String userId) throws UserNotFoundException {
        return userService.findById(userId);
    }

}
