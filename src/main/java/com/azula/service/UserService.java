package com.azula.service;

import java.util.Optional;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.azula.api.v1.dto.UserCreateDto;
import com.azula.api.v1.dto.UserLoginRequestDto;
import com.azula.api.v1.dto.UserLoginRequestResponseDto;
import com.azula.api.v1.mapper.UserMapper;
import com.azula.authentication.PBKDF2Encoder;
import com.azula.authentication.TokenUtils;
import com.azula.entity.Role;
import com.azula.entity.User;
import com.azula.exception.model.BadCredentialsException;
import com.azula.exception.user.UserBadCredentialsException;
import com.azula.exception.user.UserNotFoundException;
import com.azula.repository.UserRepository;

@ApplicationScoped
public class UserService {

    @Inject
    private UserRepository userRepository;

    @Inject
    PBKDF2Encoder encoder;

    @Inject
    TokenUtils tokenUtils;

    @Transactional(Transactional.TxType.REQUIRED)
    public User create(UserCreateDto userCreateDto) {
        validateUserEmail(userCreateDto.getEmail());

        User user = UserMapper.toEntity(userCreateDto)
                .setPassword(encoder.encode(userCreateDto.getPassword()))
                .setRoles(Set.of(Role.USER));

        userRepository.persist(user);

        return user;
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public UserLoginRequestResponseDto validateUserCredentials(UserLoginRequestDto userLoginRequestDto) throws UserNotFoundException,
            BadCredentialsException {

        User user = findByEmail(userLoginRequestDto.getUserEmail());

        if (user.getPassword().equals(encoder.encode(userLoginRequestDto.getUserPassword()))) {
            try {
                return new UserLoginRequestResponseDto()
                        .setToken(tokenUtils.generateToken(user.getEmail(), user.getRoles()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        throw new UserBadCredentialsException(userLoginRequestDto.getUserEmail());
    }

    public User findById(String userId) throws UserNotFoundException {
        return userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }

    public User findByEmail(String userEmail) throws UserNotFoundException {
        return userRepository.findByEmail(userEmail)
                .orElseThrow(() -> new UserNotFoundException(userEmail));
    }

    private void validateUserEmail(String email) {
        Optional<User> userEmail = userRepository.findByEmail(email);

        if (userEmail.isPresent()) {
            //trhow exception
        }
    }
}
