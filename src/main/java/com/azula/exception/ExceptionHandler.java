package com.azula.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.azula.exception.model.BaseException;

@Provider
public class ExceptionHandler implements ExceptionMapper<BaseException> {

    @Override
    public Response toResponse(BaseException e) {
        return Response.status(e.getStatusCode())
                .entity(e.getMessage())
                .build();
    }
}
