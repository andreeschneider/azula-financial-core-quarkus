package com.azula.exception.user;

import com.azula.exception.model.NotFoundException;

public class UserNotFoundException extends NotFoundException {

    public UserNotFoundException(String id) {
        super("Não foi encontrado usuário com o identificador " + id);
    }
}
