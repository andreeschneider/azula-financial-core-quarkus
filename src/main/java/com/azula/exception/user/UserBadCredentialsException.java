package com.azula.exception.user;

import com.azula.exception.model.BadCredentialsException;

public class UserBadCredentialsException extends BadCredentialsException {

    public UserBadCredentialsException(String userEmail) {
        super("Senha inválida para o email: " + userEmail);
    }
}
