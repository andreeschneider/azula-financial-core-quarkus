package com.azula.exception.model;

import javax.ws.rs.core.Response;

public class BadCredentialsException extends BaseException {
    public static final int STATUS_CODE = Response.Status.UNAUTHORIZED.getStatusCode();

    public BadCredentialsException(String message) {
        super(message, STATUS_CODE);
    }
}
