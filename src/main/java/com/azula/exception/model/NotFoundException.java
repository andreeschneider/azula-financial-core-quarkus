package com.azula.exception.model;

import javax.ws.rs.core.Response.Status;

public class NotFoundException extends BaseException {
    public static final int STATUS_CODE = Status.NOT_FOUND.getStatusCode();

    public NotFoundException(String message) {
        super(message, STATUS_CODE);
    }
}
