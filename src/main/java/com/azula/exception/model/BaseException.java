package com.azula.exception.model;

import java.io.Serializable;

public class BaseException extends Exception implements Serializable {
    private static final long serialVersionUID = 1L;

    private int statusCode;

    public BaseException() {
        super();
    }

    public BaseException(String msg, int statusCode) {
        super(msg);
        this.statusCode = statusCode;
    }

    public BaseException(String msg, Exception e) {
        super(msg, e);
    }

    public int getStatusCode() {
        return statusCode;
    }
}