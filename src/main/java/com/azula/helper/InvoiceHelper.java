package com.azula.helper;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.azula.entity.billing.CreditCard;
import com.azula.entity.billing.Invoice;

public class InvoiceHelper {

    public static List<Invoice> generateFirstInvoiceList(CreditCard creditCard) {
        return Arrays.asList(generateFirstInvoice(creditCard));
    }

    public static Invoice generateFirstInvoice(CreditCard creditCard) {
        return new Invoice()
                .setPeriodStart(getInvoiceStartPeriod(creditCard.getInvoiceDueDate(),
                        creditCard.getInvoiceCutDays()))
                .setCloseDate(creditCard.getInvoiceDueDate())
                .setPeriodEnd(getInvoiceEndPeriod(creditCard.getInvoiceDueDate(), creditCard.getInvoiceCutDays()))
                .setDueDate(creditCard.getInvoiceDueDate())
                .setValue(BigDecimal.ZERO)
                .setCreditCard(creditCard);
    }

    public static LocalDate getInvoiceStartPeriod(LocalDate dueDate, Long cutDays) {
        LocalDate referenceDate = LocalDate.now();

        if (referenceDate.isAfter(dueDate)) {
            return dueDate.minusDays(cutDays)
                    .minusMonths(1)
                    .plusDays(1);
        } else {
            return dueDate.minusDays(cutDays)
                    .plusDays(1);
        }
    }

    public static LocalDate getInvoiceEndPeriod(LocalDate dueDate, Long cutDays) {
        return dueDate.minusDays(cutDays);
    }
}
