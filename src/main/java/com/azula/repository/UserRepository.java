package com.azula.repository;

import com.azula.api.v1.dto.UserCreateDto;
import com.azula.entity.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {

    public Optional<User> findByEmail(String email){
        return find("email", email).firstResultOptional();
    }

    public Optional<User> findById(String id){
        return find("id", id).firstResultOptional();
    }

}
