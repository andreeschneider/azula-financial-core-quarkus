package com.azula.repository;

import javax.enterprise.context.ApplicationScoped;

import com.azula.entity.billing.CreditCard;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class CreditCardRepository implements PanacheRepository<CreditCard> {

}
