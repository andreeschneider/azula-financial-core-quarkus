package com.azula.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.azula.entity.billing.Invoice;

@Entity
@Table(name = "ACCOUNT_MOVEMENT")
public class AccountMovement {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "MOVEMENT_DATE", nullable = false)
    private LocalDateTime movementDate;

    @Column(name = "VALUE", nullable = false)
    private BigDecimal movementValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID", nullable = false, updatable = false,
            foreignKey = @ForeignKey(name = "FK_ACCOUNT_MOVEMENT_ID"))
    private Account account;

    public String getId() {
        return id;
    }

    public AccountMovement setId(String id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public AccountMovement setDescription(String description) {
        this.description = description;
        return this;
    }

    public LocalDateTime getMovementDate() {
        return movementDate;
    }

    public AccountMovement setMovementDate(LocalDateTime movementDate) {
        this.movementDate = movementDate;
        return this;
    }

    public BigDecimal getMovementValue() {
        return movementValue;
    }

    public AccountMovement setMovementValue(BigDecimal movementValue) {
        this.movementValue = movementValue;
        return this;
    }

    public Account getAccount() {
        return account;
    }

    public AccountMovement setAccount(Account account) {
        this.account = account;
        return this;
    }
}
