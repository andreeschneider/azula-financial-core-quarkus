package com.azula.entity;

public enum Role {
    USER,
    ADMIN
}
