package com.azula.entity;

public enum AccountType {
    CHECKING_ACCOUNT,
    SAVINGS,
    CREDIT_CARD,
    CORPORATE_ACCOUNT
}
