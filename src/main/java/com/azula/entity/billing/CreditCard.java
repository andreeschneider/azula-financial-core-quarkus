package com.azula.entity.billing;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.azula.entity.User;

@Entity
@Table(name = "CREDIT_CARD")
public class CreditCard {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @Column(name = "BANK_NAME", nullable = false)
    private String bankName;

    @Column(name = "INVOICE_DUE_DATE", nullable = false)
    private LocalDate invoiceDueDate;

    @Column(name = "INVOICE_CUT_DAYS")
    private Long invoiceCutDays;

    @OrderBy("invoiceId DESC")
    @OneToMany(mappedBy = "creditCard", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<Invoice> invoices;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false, updatable = false,
            foreignKey = @ForeignKey(name = "FK_CREDIT_CARD_USER_ID"))
    private User user;

    public String getId() {
        return id;
    }

    public CreditCard setId(String id) {
        this.id = id;
        return this;
    }

    public String getBankName() {
        return bankName;
    }

    public CreditCard setBankName(String bankName) {
        this.bankName = bankName;
        return this;
    }

    public LocalDate getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public CreditCard setInvoiceDueDate(LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
        return this;
    }

    public Long getInvoiceCutDays() {
        return invoiceCutDays;
    }

    public CreditCard setInvoiceCutDays(Long invoiceCutDays) {
        this.invoiceCutDays = invoiceCutDays;
        return this;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public CreditCard setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
        return this;
    }

    public User getUser() {
        return user;
    }

    public CreditCard setUser(User user) {
        this.user = user;
        return this;
    }
}
