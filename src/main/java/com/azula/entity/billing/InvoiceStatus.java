package com.azula.entity.billing;

public enum InvoiceStatus {
    OPEN,
    CLOSED,
    PAYED
}
