package com.azula.entity.billing;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ORDER_INSTALLMENT")
public class OrderInstallment {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @Column(name = "REFERENCE_PERIOD", nullable = false)
    private LocalDate referencePeriod;

    @Column(name = "INSTALLMENT_SEQUENCE", nullable = false)
    private Integer sequence;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_INSTALLMENT_ORDER_ID", referencedColumnName = "ID", nullable = false, updatable = false,
            foreignKey = @ForeignKey(name = "FK_ORDER_INSTALLMENT_ORDER_ID"))
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_INSTALLMENT_INVOICE_ID", referencedColumnName = "ID", nullable = false, updatable = false,
            foreignKey = @ForeignKey(name = "FK_ORDER_INSTALLMENT_INVOICE_ID"))
    private Invoice invoice;

    public String getId() {
        return id;
    }

    public OrderInstallment setId(String id) {
        this.id = id;
        return this;
    }

    public LocalDate getReferencePeriod() {
        return referencePeriod;
    }

    public OrderInstallment setReferencePeriod(LocalDate referencePeriod) {
        this.referencePeriod = referencePeriod;
        return this;
    }

    public Integer getSequence() {
        return sequence;
    }

    public OrderInstallment setSequence(Integer sequence) {
        this.sequence = sequence;
        return this;
    }

    public Order getOrder() {
        return order;
    }

    public OrderInstallment setOrder(Order order) {
        this.order = order;
        return this;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public OrderInstallment setInvoice(Invoice invoice) {
        this.invoice = invoice;
        return this;
    }
}

