package com.azula.entity.billing;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "INVOICES")
public class Invoice {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @Column(name = "PERIOD_START", nullable = false)
    private LocalDate periodStart;

    @Column(name = "PERIOD_END", nullable = false)
    private LocalDate periodEnd;

    @Column(name = "CLOSE_DATE", nullable = false)
    private LocalDate closeDate;

    @Column(name = "DUE_DATE", nullable = false)
    private LocalDate dueDate;

    @Column(name = "VALUE", nullable = false)
    private BigDecimal value;

    @OneToMany(mappedBy = "invoice", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<OrderInstallment> orders;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREDIT_CARD_ID", referencedColumnName = "ID", nullable = false, updatable = false,
            foreignKey = @ForeignKey(name = "FK_CREDIT_CARD_INVOICE_ID"))
    private CreditCard creditCard;

    public String getId() {
        return id;
    }

    public Invoice setId(String id) {
        this.id = id;
        return this;
    }

    public LocalDate getPeriodStart() {
        return periodStart;
    }

    public Invoice setPeriodStart(LocalDate periodStart) {
        this.periodStart = periodStart;
        return this;
    }

    public LocalDate getPeriodEnd() {
        return periodEnd;
    }

    public Invoice setPeriodEnd(LocalDate periodEnd) {
        this.periodEnd = periodEnd;
        return this;
    }

    public LocalDate getCloseDate() {
        return closeDate;
    }

    public Invoice setCloseDate(LocalDate closeDate) {
        this.closeDate = closeDate;
        return this;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public Invoice setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Invoice setValue(BigDecimal value) {
        this.value = value;
        return this;
    }

    public List<OrderInstallment> getOrders() {
        return orders;
    }

    public Invoice setOrders(List<OrderInstallment> orders) {
        this.orders = orders;
        return this;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public Invoice setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
        return this;
    }
}
