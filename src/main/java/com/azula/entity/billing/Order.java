package com.azula.entity.billing;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ORDERS")
public class Order {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @Column(name = "DATE", nullable = false)
    private LocalDateTime date;

    @Column(name = "VALUE", nullable = false)
    private BigDecimal movementValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVOICE_ORDER_ID", referencedColumnName = "ID", nullable = false, updatable = false,
            foreignKey = @ForeignKey(name = "FK_ORDER_INVOICE_ID"))
    private Invoice invoice;

    @OneToMany(mappedBy = "order", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<OrderInstallment> installments;

    public String getId() {
        return id;
    }

    public Order setId(String id) {
        this.id = id;
        return this;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Order setDate(LocalDateTime date) {
        this.date = date;
        return this;
    }

    public BigDecimal getMovementValue() {
        return movementValue;
    }

    public Order setMovementValue(BigDecimal movementValue) {
        this.movementValue = movementValue;
        return this;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public Order setInvoice(Invoice invoice) {
        this.invoice = invoice;
        return this;
    }

    public List<OrderInstallment> getInstallments() {
        return installments;
    }

    public Order setInstallments(List<OrderInstallment> installments) {
        this.installments = installments;
        return this;
    }
}
