package com.azula;

import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.azula.authentication.TokenUtils;
import com.azula.entity.Role;

@ApplicationScoped
public class RequestSpecificationBuilder {

    @Inject
    TokenUtils tokenUtils;

    public String genericUserToken() {
        try {
            return "Bearer " + tokenUtils.generateToken("johnDoe@gmail.com", Set.of(Role.USER));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
