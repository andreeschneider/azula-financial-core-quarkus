package com.azula.helper;

import java.time.LocalDate;

import com.azula.api.v1.dto.CreditCardCreateDto;

public class CreditCardTestHelper {

    public static CreditCardCreateDto create(String bankName, Long cutDays, LocalDate dueDate, String userId) {
        return new CreditCardCreateDto()
                .setBankName(bankName)
                .setInvoiceCutDays(cutDays)
                .setInvoiceDueDate(dueDate)
                .setUserIdentification(userId);
    }
}
