package com.azula.helper;

import com.azula.api.v1.dto.UserCreateDto;

public class UserTestHelper {

    public static UserCreateDto createUser() {
        return createUser("john.doe@gmail.com", "John Doe", "1234");
    }

    public static UserCreateDto createUser(String email, String name, String password) {
        return new UserCreateDto()
                .setEmail(email)
                .setName(name)
                .setPassword(password);
    }
}
