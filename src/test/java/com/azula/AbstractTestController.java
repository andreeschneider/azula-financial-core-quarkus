package com.azula;

import javax.inject.Inject;

import com.azula.api.v1.controller.RestPath;

public abstract class AbstractTestController {

    public static String API_V1;
    public static String USER_RESOURCE;
    public static String CREDIT_CARD_APP_RESOURCE;

    @Inject
    protected RequestSpecificationBuilder requestSpecificationBuilder;

    public static void setup() {
        API_V1 = RestPath.BASE_PATH;
        USER_RESOURCE = API_V1 + "/users";
        CREDIT_CARD_APP_RESOURCE = API_V1 + "/credit-card";
    }
}
