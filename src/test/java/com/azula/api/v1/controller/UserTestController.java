package com.azula.api.v1.controller;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.azula.AbstractTestController;
import com.azula.api.v1.dto.UserCreateDto;
import com.azula.api.v1.dto.UserLoginRequestDto;
import com.azula.api.v1.dto.UserLoginRequestResponseDto;
import com.azula.api.v1.dto.UserResponseDto;
import com.azula.helper.UserTestHelper;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
public class UserTestController extends AbstractTestController {

    @BeforeAll
    public static void init() {
        setup();
    }

    @Test
    public void create() {
        UserCreateDto userCreateDto = new UserCreateDto()
                .setEmail("johndoe@gmail.com")
                .setName("John Doe")
                .setPassword("1234");

        UserResponseDto userResponseDto = createUser(userCreateDto);

        assertCreatedUser(userCreateDto, userResponseDto);
    }

    @Test
    public void login() {
        UserCreateDto userCreateDto = new UserCreateDto()
                .setEmail("johndoe@gmail.com")
                .setName("John Doe")
                .setPassword("1234");

        createUser(userCreateDto);

        UserLoginRequestDto userLoginRequestDto = new UserLoginRequestDto()
                .setUserEmail(userCreateDto.getEmail())
                .setUserPassword(userCreateDto.getPassword());

        UserLoginRequestResponseDto userLoginRequestResponseDto = doLogin(userLoginRequestDto);

        assertNotNull(userLoginRequestResponseDto.getToken());
    }

    private void assertCreatedUser(UserCreateDto createDto, UserResponseDto userResponseDto) {
        assertEquals(createDto.getEmail(), userResponseDto.getEmail());
        assertEquals(createDto.getName(), userResponseDto.getName());

        assertNotNull(userResponseDto.getId());
    }

    private UserLoginRequestResponseDto doLogin(UserLoginRequestDto userLoginRequestDto) {
        return given()
                .header("Content-Type", "application/json")
                .body(userLoginRequestDto)
                .post(USER_RESOURCE + "/login")
                .then()
                .statusCode(200)
                .extract()
                .as(UserLoginRequestResponseDto.class);
    }

    private static UserResponseDto createUser(UserCreateDto userCreateDto) {
        return given()
                .header("Content-Type", "application/json")
                .body(userCreateDto)
                .post(USER_RESOURCE)
                .then()
                .statusCode(200)
                .extract()
                .as(UserResponseDto.class);
    }

}
