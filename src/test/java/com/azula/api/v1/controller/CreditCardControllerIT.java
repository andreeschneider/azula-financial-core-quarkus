package com.azula.api.v1.controller;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.requestSpecification;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.azula.AbstractTestController;
import com.azula.RequestSpecificationBuilder;
import com.azula.api.v1.dto.CreditCardCreateDto;
import com.azula.api.v1.dto.CreditCardResponseDto;
import com.azula.api.v1.dto.UserCreateDto;
import com.azula.api.v1.dto.UserResponseDto;
import com.azula.helper.CreditCardTestHelper;
import com.azula.helper.UserTestHelper;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
public class CreditCardControllerIT extends AbstractTestController {

    @BeforeAll
    public static void init() {
        setup();
    }

    @Test
    public void create() {
        UserResponseDto userResponseDto = createUser(UserTestHelper.createUser());

        CreditCardCreateDto creditCardCreateDto = CreditCardTestHelper
                .create("Nubank", 15l, LocalDate.now(), userResponseDto.getId());

        CreditCardResponseDto creditCardResponseDto = given()
                .header("Content-Type", "application/json")
                .header("Authorization", requestSpecificationBuilder.genericUserToken())
                .body(creditCardCreateDto)
                .post(CREDIT_CARD_APP_RESOURCE)
                .then()
                .statusCode(200)
                .extract()
                .as(CreditCardResponseDto.class);

        assertCreatedCreditCard(creditCardCreateDto, creditCardResponseDto);
    }

    private static UserResponseDto createUser(UserCreateDto userCreateDto) {
        return given()
                .header("Content-Type", "application/json")
                .body(userCreateDto)
                .post(USER_RESOURCE)
                .then()
                .statusCode(200)
                .extract()
                .as(UserResponseDto.class);
    }

    private void assertCreatedCreditCard(CreditCardCreateDto creditCardCreateDto, CreditCardResponseDto creditCardResponseDto) {
        assertNotNull(creditCardResponseDto.getId());

        assertEquals(creditCardCreateDto.getBankName(), creditCardResponseDto.getBankName());
        assertEquals(creditCardCreateDto.getUserIdentification(), creditCardResponseDto.getUserIdentification());
        assertEquals(creditCardCreateDto.getInvoiceCutDays(), creditCardResponseDto.getInvoiceCutDays());
        assertEquals(creditCardCreateDto.getInvoiceDueDate(), creditCardResponseDto.getInvoiceDueDate());
    }
}
